/* toggle dropdowns */

if ($(window).width() > 1199) {
    $(".toggle-item").click(function () {
        var i = $(this).attr("data-slide");

        if ($(this).hasClass("active")) {

            $(this).removeClass("active");
            $(i).fadeOut()

        } else {

            $(".toggle-item").removeClass("active");
            $(".dropdown-content").fadeOut();

            $(this).addClass("active");
            $(i).fadeIn();
        }

    });


    /* active / inactive */
    $(".lvl2-categ").click(function () {
        if ($(this).hasClass("active")) {

            $(this).removeClass("active");

        } else {

            $(this).addClass("active");

        }
    })

    $(".categ").hover(function () {
        $(".lvl2-categ ", this).click();
    })



}
$(".close-toggle").click(function () {
    var i = $(this).attr("data-slide");
    $(i).fadeOut()
    $("a[data-slide='" + i + "']").removeClass("active");
})
$(".toggle-m").click(function () {
    var i = $(this).attr("data-slide");

    if ($(this).hasClass("active")) {

        $(this).removeClass("active");
        $(i).fadeOut()

    } else {

        $(".toggle-m").removeClass("active");
        $(".dropdown-content").fadeOut();

        $(this).addClass("active");
        $(i).fadeIn();
    }

});
$(".remove-m-toggle").each(function () {

    if ($(window).width() > 1199) {
        $(this).attr("href", "javascript:void(0);");

    }
    else {

        $(this).attr("href", $(this).attr("data-href"));
    }

})


if ($(window).width() < 993) {
    $(".categorii_tip").click(function () {
        var i = $(this).attr("data-slide");

        if ($(this).hasClass("active")) {

            $(this).removeClass("active");
            $(i).fadeOut()

        } else {

            $(".categorii_tip").removeClass("active");
            $(".categorii_tip_expand").fadeOut();

            $(this).addClass("active");
            $(i).fadeIn();
        }

    });
}

